#ifndef _CIRC_BUF_H_
#define _CIRC_BUF_H_

typedef struct {
	char *start, *end;
	char *head, *tail;
} CircBuf;

void circ_init(CircBuf *buf, char *start, int size);

static inline char circ_isEmpty(CircBuf *buf) {
	return buf->head == buf->tail;
}

static inline int circ_getSize(CircBuf *buf) {
	return buf->head - buf->tail + (buf->head >= buf->tail ? 0 : buf->end - buf->start + 1);
}

/**
 * if the buffer isn't empty, reads a character from it, and removes the character
 * otherwise, returns the null character
 */
char circ_getc(CircBuf *buf);

/**
 * if the buffer isn't empty, returns a character from it without removing the character
 * otherwise, returns the null character
 */
char circ_peek(CircBuf *buf);

/**
 * removes the most recently added character
 */
void circ_delete(CircBuf *buf);

/**
 * if the buffer has space, writes a character to it, and returns true
 * otherwise, returns false and leaves the buffer as is
 */
char circ_putc(CircBuf *buf, char c);

/** 
 * inserts c at the tail of the buffer, so it is read first
 * NOTE: will over-write the tail if the buffer is full
 * this is so that we can be certain the solenoid gets turned off quickly
 */
void circ_insertFromTail(CircBuf *buf, char c, int ind);

char circ_write(CircBuf *buf, char *str);

void circ_clear(CircBuf *buf);

#endif //_CIRC_BUF_H_
