#ifndef _PARSER_H_
#define _PARSER_H_

#include "circ_buf.h"

void parse_command(CircBuf *cmd, CircBuf *buf_out, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int *train_speeds, unsigned int upTime);

#endif //_PARSER_H_
