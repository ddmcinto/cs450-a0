#ifndef _SENSORS_H_
#define _SENSORS_H_

#include "circ_buf.h"

void sensors_update(CircBuf *term_out, CircBuf *train_out, CircBuf *train_in, unsigned int upTime);

#endif //_SENSORS_H_
