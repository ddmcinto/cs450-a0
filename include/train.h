#ifndef _TRAIN_H_
#define _TRAIN_H_

#include "circ_buf.h"

void train_init(CircBuf *train_out, unsigned int *train_speeds);

void train_setSpeed(CircBuf *train_out, char train, char speed, unsigned int *train_speeds);

void train_initReverse(CircBuf *train_out, char train, int *status, unsigned int *train_timeouts, unsigned int upTime);

void train_manageReversals(CircBuf *term_out, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int *train_speeds, unsigned int upTime);

#endif
