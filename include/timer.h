#ifndef _TIMER_H_
#define _TIMER_H_

#include "circ_buf.h"

#define CLOCK_LEN (sizeof "00:00:00.000")

void time_init();
void time_write(CircBuf *term_out, unsigned int *upTime);

#endif //_TIMER_H_
