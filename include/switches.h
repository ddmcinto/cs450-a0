#ifndef _SWITCHES_H_
#define _SWITCHES_H_

#include "circ_buf.h"

void switches_init(CircBuf *term_out, CircBuf *train_out);

void switches_print(CircBuf *term_out);

/**
 * updates the stored status of a switch, and returns
 * an integer with bitflags representing the status
 * 
 * switch i corresponds to bit (i & 0x1F)
 *
 * Tip: to get the status integer without loosing the existing status,
 * call this with a switch value such as 23 that isn't a valid switch
 */
int switches_status(char sw, char dir);

void switches_set(CircBuf *train_out, char sw, char dir);

#endif
