#ifndef _UTILS_H_
#define _UTILS_H_

#define RUNNING_MASK  0x1
#define SENDING_CMD_MASK 0x2
#define SOLENOID_MASK 0x4
#define TRAIN_REV_MASK 0x8
#define REDRAW_SWITCHES 0x10


#define SOLENOID_TIMEOUT 3

#define TIMER_FREQUENCY 508000 //508 kHz

#endif //_UTILS_H_
