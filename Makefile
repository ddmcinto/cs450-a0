#
# Makefile for busy-wait IO library
#
XCC     = gcc
AS	= as
LD	= ld
CFLAGS  = -c -fPIC -Wall -Isrc -Iinclude -Ilibs/include -mcpu=arm920t -msoft-float
# -g: include hooks for gdb
# -c: only compile
# -mcpu=arm920t: generate code for the 920t architecture
# -fpic: emit position-independent code
# -Wall: report all warnings
# -msoft-float: use software for floating point

ASFLAGS	= -mcpu=arm920t -mapcs-32
# -mapcs-32: always create a complete stack frame

LDFLAGS = -init main -Map helloWorld.map -N  -T orex.ld -L/u/wbcowan/gnuarm-4.0.2/lib/gcc/arm-elf/4.0.2 -Llibs

all: build/helloWorld.elf .install

src/%.s: src/%.c 
	$(XCC) -S $(CFLAGS) -o $@ $^

build/%.o: src/%.s
	$(AS) $(ASFLAGS) -o $@ $^

build/helloWorld.elf: build/helloWorld.o build/circ_buf.o build/timer.o build/parser.o build/sensors.o build/utils.o build/switches.o build/train.o
	$(LD) $(LDFLAGS) -o $@ $^ -lbwio -lgcc

.install:
	cp build/helloWorld.elf /u/cs452/tftp/ARM/ddmcinto

clean:
	-rm -f build/helloWorld.elf src/*.s build/*.o build/helloWorld.map
# mark all intermediates as secondary to prevent the removal of the .s files
.SECONDARY:
