#include "sensors.h"
#include "circ_buf.h"

void output_sensor(CircBuf *term_out, int cur_cluster, int vals) {
	circ_putc(term_out, 'A' + cur_cluster);
	circ_putc(term_out, ' ');

	int i;
	for (i = (1<<15); i > 0; i >>= 1) {
		circ_putc(term_out, (vals & i) ? '1' : '0');
	}

}

void sensors_update(CircBuf *term_out, CircBuf *train_out, CircBuf *train_in, unsigned int upTime) {
	static unsigned int lastQuery = 0;

	if (upTime - lastQuery > 1) {
		circ_putc(train_out, 0x85);
		circ_clear(train_in);
		lastQuery = upTime;
	} else if (circ_getSize(train_in) >= 10) {
		circ_write(term_out, "\0337\033[2;60H\033[K\n");
		int i;
		for (i = 0; i < 5; ++i) {
			int vals = (circ_getc(train_in) << 8) | circ_getc(train_in);
			output_sensor(term_out, i, vals);
			circ_write(term_out, "\n\033[18D");
		}

		circ_write(term_out, "\0338");
	}
}
