#include "circ_buf.h"
#include "ts7200.h"
#include "parser.h"
#include "utils.h"
#include "switches.h"
#include "train.h"

inline void handleInvalidCommand(CircBuf *term_out) {
	circ_write(term_out, "Invalid Command\n\r");
}

int parseInt(CircBuf *cmd, char *next) {
	if (circ_isEmpty(cmd)) {
		return 0;
	}
	int val = 0;
	char c = circ_getc(cmd);
	while (c >= '0' && c <= '9') {
		val *= 10;
		val += c - '0';
		if (circ_isEmpty(cmd)) {
			return val;
		}
		c = circ_getc(cmd);
	}
	*next = c;
	return val;
}

void parse_train(CircBuf *cmd, CircBuf *term_out, CircBuf *train_out, unsigned int *train_speeds) {
	if (circ_getc(cmd) != 'r' ||
		circ_getc(cmd) != ' ') {
		handleInvalidCommand(term_out);
		return;
	}

	char c;
	int train = parseInt(cmd, &c);
	if (c != ' ') {
		handleInvalidCommand(term_out);
		return;
	}

	c = 0;
	int speed = parseInt(cmd, &c);
	if (!circ_isEmpty(cmd) || c) {
		handleInvalidCommand(term_out);
		return;
	}

	circ_write(term_out, "Set to speed ");
	circ_putc(term_out, 'a' + speed);
	circ_write(term_out, "\n\r");

	train_setSpeed(train_out, train, speed, train_speeds);
}

void parse_reverse(CircBuf *cmd, CircBuf *term_out, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int upTime) {
	if (circ_getc(cmd) != 'v' ||
		circ_getc(cmd) != ' ') {
		handleInvalidCommand(term_out);
		return;
	}

	char c = 0;
	char train = (char)parseInt(cmd, &c);
	if (!circ_isEmpty(cmd) || c) {
		handleInvalidCommand(term_out);
		return;
	}

	train_initReverse(train_out, train, status, train_timeouts, upTime);
}

void parse_switch(CircBuf *cmd, CircBuf *term_out, CircBuf *train_out) {
	if (circ_getc(cmd) != 'w' ||
		circ_getc(cmd) != ' ') {
		handleInvalidCommand(term_out);
		return;
	}

	char c;
	char sw = parseInt(cmd, &c) & 0xFF;
	if (c != ' ') {
		handleInvalidCommand(term_out);
		return;
	}

	char dir = circ_getc(cmd) | 0x20; //to lowercase
	if (dir != 'c' && dir != 's') {
		handleInvalidCommand(term_out);
		return;
	}
	if (!circ_isEmpty(cmd)) {
		handleInvalidCommand(term_out);
		return;
	}

	switches_set(train_out, sw, dir);
}

void parse_command(CircBuf *cmd, CircBuf *term_out, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int *train_speeds, unsigned int upTime) {
	char first = circ_getc(cmd);
	switch (first) {
		case 't':
		{
			parse_train(cmd, term_out, train_out, train_speeds);
			break;
		}
		case 'r':
		{
			parse_reverse(cmd, term_out, train_out, status, train_timeouts, upTime);
			break;
		}
		case 's':
		{
			parse_switch(cmd, term_out, train_out);
			break;
		}
		case 'q':
		{
			if (circ_isEmpty(cmd)) {
				*status &= ~RUNNING_MASK;
			}
			else {
				handleInvalidCommand(term_out);
			}
			break;
		}
		default:
			handleInvalidCommand(term_out);
			break;
	}
}
