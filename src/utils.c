

void * memcpy ( void * destination, const void * source, unsigned int num ) {
	void * toReturn = destination;
	while(num--) {
		*((char *)destination++) = *((char *)source++);
	}
	return toReturn;
}
