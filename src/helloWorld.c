#include "ts7200.h"
#include "bwio.h"
#include "circ_buf.h"
#include "timer.h"
#include "parser.h"
#include "sensors.h"
#include "utils.h"
#include "switches.h"
#include "train.h"

//make sure cache is off, and cpu is not set to higher speed
//reed uart error reg

#define TRAIN_BAUD 2400
#define F_UART_CLK 7372800

#define DEBUG_TIME 0

void config_train_uart() {
	volatile int *LCRL = (int *)( UART1_BASE + UART_LCRL_OFFSET );
	volatile int *LCRM = (int *)( UART1_BASE + UART_LCRM_OFFSET );
	volatile int *LCRH = (int *)( UART1_BASE + UART_LCRH_OFFSET );
	volatile int *CTLR = (int *)( UART1_BASE + UART_CTLR_OFFSET );

	//divisor
	int brd = (F_UART_CLK / (TRAIN_BAUD * 16)) - 1;
	//set speed to 2400
	*LCRM = brd >> 8;
	*LCRL = brd & 0xFF;

	//set remaining flags
	//a write to LCRH MUST happen, in order for LCRL and LCRM to be updated
	*LCRH &= ~FEN_MASK;
	*LCRH |= STP2_MASK;

	*CTLR |= UARTEN_MASK;// | TIEN_MASK | RIEN_MASK | MSIEN_MASK | RTIEN_MASK;

	//we use timer2 to limit the rate at which we send commands to the train
	int *TIME_LDR = (int *)(TIMER2_BASE + LDR_OFFSET);
	//limit to 1 byte per 30-60 ms
	*TIME_LDR = (int)(TIMER_FREQUENCY*0.030);
	int *TIME_CRTL = (int *)(TIMER2_BASE + CRTL_OFFSET);
	*TIME_CRTL |= ENABLE_MASK | MODE_MASK | CLKSEL_MASK;
}

unsigned int train_out_limiter() {
	static unsigned int total = 0;
	static int prev_val = 0;
	volatile int *val_ptr = (int *)(TIMER2_BASE + VAL_OFFSET);
	int new_val = *val_ptr;
	if (new_val > prev_val)
	{
		++total;
	}
	prev_val = new_val;
	return total;
}

void term_output(CircBuf *term_out) {
	volatile int *const term_flags = (int *)( UART2_BASE + UART_FLAG_OFFSET );
	volatile int *const term_data = (int *)( UART2_BASE + UART_DATA_OFFSET );

	//check if we have to write to terminal
	if (!circ_isEmpty(term_out) && !( *term_flags & TXFF_MASK )) {
		*term_data = circ_getc(term_out);
	}
}

void term_input(CircBuf *term_out, CircBuf *term_in, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int *train_speeds, unsigned int upTime) {
	volatile int *const term_flags = (int *)( UART2_BASE + UART_FLAG_OFFSET );
	volatile int *const term_data = (int *)( UART2_BASE + UART_DATA_OFFSET );

	//handle input
	if ( !(*term_flags & RXFE_MASK ) ) {
		char input = (*term_data) & DATA_MASK;
		if (input == '\b') {
			if (!circ_isEmpty(term_in)) {
				circ_delete(term_in);
			}
			circ_write(term_out, "\b \b");
		}
		else if (input == '\r' || input == '\r') {
			circ_write(term_out, "\r\n");
			parse_command(term_in, term_out, train_out, status, train_timeouts, train_speeds, upTime);
			circ_clear(term_in);
			*status |= REDRAW_SWITCHES;
		}
		else if ((input >= ' ' && input <= '~') || input == '\033'){
			circ_putc(term_in, input);
			circ_putc(term_out, input);
		}
	}
}

void train_io(CircBuf *term_out, CircBuf *train_out, CircBuf *train_in, int *status) {
	volatile int *const train_flags = (int *)( UART1_BASE + UART_FLAG_OFFSET );
	volatile int *const train_data = (int *)( UART1_BASE + UART_DATA_OFFSET );

	static unsigned int last_send = 0;
	unsigned int new_send = train_out_limiter();

	if (!circ_isEmpty(train_out) && !( *train_flags & TXFF_MASK ) && ( *train_flags & CTS_MASK ) && new_send > last_send + 1) {
		char c = circ_getc(train_out);
		*train_data = c;

		//update status
		if (*status & SENDING_CMD_MASK) {
			*status &= ~SENDING_CMD_MASK;
		}
		else if (c <= 0x1F) {
			*status |= SENDING_CMD_MASK;
		}
		else if (c == 0x21 || c == 0x22) {
			*status |= SENDING_CMD_MASK | SOLENOID_MASK;
		}
		last_send = new_send;
	}

	if ( !(*train_flags & RXFE_MASK ) ) {
		char input = (*train_data) & DATA_MASK;
		circ_putc(train_in, input);
	}
}

void init_loop(CircBuf *term_out, CircBuf *term_in, CircBuf *train_out, CircBuf *train_in, int *status, unsigned int *train_speeds) {
	circ_write(term_out, "initializing .");

	char initializing = 1;
	unsigned int prev_upTime = 0;
	unsigned int upTime = 0; //use a separate upTime for initialization
	while(initializing) {
		term_output(term_out);
		train_io(term_out, train_out, train_in, status);
		
		if (upTime > prev_upTime) {
			circ_putc(term_out, '.');

			if (upTime <= 18) {
				switches_set(train_out, upTime, 'c');
			}
			else if (upTime <= 22) {
				switches_set(train_out, (153 - 19) + upTime, 's');
			} else if (upTime < 22 + SOLENOID_TIMEOUT) {				
			} else if (upTime == 22 + SOLENOID_TIMEOUT) {
				circ_putc(train_out, 0x20);
			}
			else if (upTime <= (102 + SOLENOID_TIMEOUT)) {
				train_setSpeed(train_out, upTime - (22 + SOLENOID_TIMEOUT), 0, train_speeds);
			}
			else {
				initializing = 0;
			}
		}

		prev_upTime = upTime;
		time_write(term_out, &upTime);
	}

	circ_write(term_out, "\rinitializing .\033[K");

	while(!circ_isEmpty(train_out)) {
		term_output(term_out);
		train_io(term_out, train_out, train_in, status);
		
		if (upTime > prev_upTime) {
			circ_putc(term_out, '.');
		}

		prev_upTime = upTime;
		time_write(term_out, &upTime);
	}
	circ_write(term_out, "\r\033[K");
}

int main() {
	char term_out_arr [256];
	char term_in_arr [256];
	char train_out_arr [128];
	char train_in_arr [64];
	CircBuf term_out, term_in, train_out, train_in;
	circ_init(&term_out, (char *)term_out_arr, sizeof term_out_arr);
	circ_init(&term_in, (char *)term_in_arr, sizeof term_in_arr);
	circ_init(&train_out, (char *)train_out_arr, sizeof train_out_arr);
	circ_init(&train_in, (char *)train_in_arr, sizeof train_in_arr);

	config_train_uart();
	time_init();

	circ_write(&term_out, "\033[2J\033[5m\033[2;0H");
	circ_putc(&train_out, 0x60);

	int status = RUNNING_MASK | REDRAW_SWITCHES;
	unsigned int solenoid_timeout = 0;
	unsigned int train_timeouts[81];
	unsigned int train_speeds[81];
	int i;
	for (i = 0; i < 81; ++i) {
		train_timeouts[i] = (unsigned int)-1;
	}

	switches_init(&term_out, &train_out);

	unsigned int upTime = 1;
	init_loop(&term_out, &term_in, &train_out, &train_in, &status, train_speeds);

#if DEBUG_TIME
	int iterations;
	int prev_cycles = 0, next_cycles = 0, max_cycles;
	*((int*)0x80810064) |= 0x10;
	for(iterations = 0; status || solenoid_timeout >= upTime; ++iterations) {
#else
	for(; status || solenoid_timeout >= upTime;) {
#endif
		if (status & REDRAW_SWITCHES) {
			switches_print(&term_out);
			status &= ~REDRAW_SWITCHES;
		}
		term_output(&term_out);
		term_input(&term_out, &term_in, &train_out, &status, train_timeouts, train_speeds, upTime);
		train_io(&term_out, &train_out, &train_in, &status);

		//manage solenoid
		if (status & SOLENOID_MASK) {
			solenoid_timeout = upTime + SOLENOID_TIMEOUT;
			status &= ~SOLENOID_MASK;
		}
		else if (upTime == solenoid_timeout) //200ms <= t <= 300ms where t = time since last switch thrown
		{
			circ_insertFromTail(&train_out, 0x20, (status & SENDING_CMD_MASK) ? 1 : 0);
			--solenoid_timeout;
		}

		if (status & TRAIN_REV_MASK) {
			train_manageReversals(&term_out, &train_out, &status, train_timeouts, train_speeds, upTime);
		}

		sensors_update(&term_out, &train_out, &train_in, upTime);
		time_write(&term_out, &upTime);
#if DEBUG_TIME
		next_cycles = *((int*)0x80810060);
		int cycles = next_cycles - prev_cycles;
		prev_cycles = next_cycles;
		if (cycles > max_cycles) {
			max_cycles = cycles;
		}
		if (!(iterations % 98300)) {
			while (max_cycles) {
				circ_putc(&term_out, '0' + max_cycles % 10);
				max_cycles /= 10;
			}
			circ_putc(&term_out, ' ');
		}
#endif		
	}

#if DEBUG_TIME
	*((int*)0x80810064) &= ~0x10;
#endif

	bwputstr(COM2, "finishing");

	return 0;
}
