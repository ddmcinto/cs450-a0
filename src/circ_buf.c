#include "circ_buf.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

static inline char isFull(CircBuf *buf) {
	return (buf->head + 1 == buf->tail) || (buf->head == buf->end && buf->tail == buf->start);
}

void circ_init(CircBuf *buf, char *start, int size) {
	buf->start = start;
	buf->end = start+size - 1;
	buf->head = start;
	buf->tail = start;
}

char circ_getc(CircBuf *buf) {
	if (circ_isEmpty(buf)) {
		return 0;
	}

	char result = *(buf->tail);
	if (buf->tail == buf->end) {
		buf->tail = buf->start;
	}
	else {
		++(buf->tail);
	}
	return result;
}

char circ_peek(CircBuf *buf) {
	if (circ_isEmpty(buf)) {
		return 0;
	}
	return *(buf->tail);
}

void circ_delete(CircBuf *buf) {
	if (buf->head == buf->start) {
		buf->head = buf->end;
	}
	else {
		--(buf->head);
	}
}

char circ_putc(CircBuf *buf, char c) {
	if (isFull(buf)) {
		return FALSE;
	}

	*(buf->head) = c;
	if (buf->head == buf->end) {
		buf->head = buf->start;
	}
	else {
		++(buf->head);
	}
	return TRUE;
}

void circ_insertFromTail(CircBuf *buf, char c, int ind) {
	if (!isFull(buf)) {
		if (buf->tail == buf->start) {
			buf->tail = buf->end;
		}
		else {
			--(buf->tail);
		}
	}


	//used to turn off the solenoid, so we MUST
	//insert c regardless of if the buffer is full
	char *cur = buf->tail;
	char *next;
	for (; ind; --ind) {
		if (cur == buf->head) {
			next = buf->end;
		}
		else
		{
			next = cur-1;
		}
		*cur = *next;
		cur = next;
	}
	*cur = c;
}

char circ_write(CircBuf *buf, char *str) {
	//TODO optimize so checks only run once

	//write all but the terminating null character
	while(*str && circ_putc(buf, *str))
	{
		++str;
	}
	//we didn't reach the end of the string to write
	if (!*str) {
		return FALSE;
	}

	//write the terminating null character
	return circ_putc(buf, *str);
}

void circ_clear(CircBuf *buf) {
	buf->head = buf->start;
	buf->tail = buf->start;
}

