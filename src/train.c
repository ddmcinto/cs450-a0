#include "train.h"
#include "circ_buf.h"
#include "utils.h"

void train_init(CircBuf *train_out, unsigned int *train_speeds) {
	int i;
	for (i = 1; i <= 80; ++i) {
		train_setSpeed(train_out, i, 0, train_speeds);
	}
}

void setSpeed(CircBuf *train_out, char train, char speed) {
	circ_putc(train_out, speed);
	circ_putc(train_out, train);
}

void train_setSpeed(CircBuf *train_out, char train, char speed, unsigned int *train_speeds) {
	setSpeed(train_out, train, speed);
	train_speeds[train] = speed;
}

void train_initReverse(CircBuf *train_out, char train, int *status, unsigned int *train_timeouts, unsigned int upTime) {
	//*
	setSpeed(train_out, train, 0);
	/*/
	//insert a stop command
	circ_insertFromTail(train_out, 0, (*status & SENDING_CMD_MASK) ? 1 : 0);
	circ_insertFromTail(train_out, train, (*status & SENDING_CMD_MASK) ? 1 : 0);
	//*/

	train_timeouts[train] = upTime + 30;
	*status |= TRAIN_REV_MASK;
}

void train_manageReversals(CircBuf *term_out, CircBuf *train_out, int *status, unsigned int *train_timeouts, unsigned int *train_speeds, unsigned int upTime) {
	char i;
	char trains_reversing = 0;
	for (i = 1; i <= 80; ++i) {
		if (upTime >= train_timeouts[i]) {
			setSpeed(train_out, i, 15);
			setSpeed(train_out, i, train_speeds[i]);
			train_timeouts[i] = 0;
		}
		else if (train_timeouts[i] != (unsigned int)-1) {
			trains_reversing = 1;
		}
	}
	//no more trains reversing, turn off flag
	if (!trains_reversing) {
		*status &= ~TRAIN_REV_MASK;
	}
}
