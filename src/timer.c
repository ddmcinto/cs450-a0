#include "ts7200.h"
#include "timer.h"
#include "circ_buf.h"
#include "utils.h"

#define TIME_ZERO "00:00:00.0"
void time_init() {
	int *LDR = (int *)(TIMER3_BASE + LDR_OFFSET);
	*LDR = TIMER_FREQUENCY/10;
	int *CRTL = (int *)(TIMER3_BASE + CRTL_OFFSET);
	*CRTL |= ENABLE_MASK | MODE_MASK | CLKSEL_MASK;
}

void time_write(CircBuf *term_out, unsigned int *upTime) {
	//used as an int array, but initialized with a string literal since brace initialization is unreliable
	const char *moduli = "\12" "\12" "\0"	//99:
						 "\6" "\12" "\0"	//59:
						 "\6" "\12" "\0"	//59.
						 "\12"; 			// 9
	static int prev_val = 0;

	volatile int *val_ptr = (int *)(TIMER3_BASE + VAL_OFFSET);
	int new_val = *val_ptr;
	if (new_val > prev_val) {
		++(*upTime);

		circ_write(term_out, "\0337\033[H");

		unsigned int upTime_cpy = *upTime;
		char time_str[sizeof TIME_ZERO];

		int i;
		for (i = sizeof time_str - 1; i >= 0; --i) {
			if (moduli[i] == 0) {
				time_str[i] = TIME_ZERO[i];
			}
			else {
				time_str[i] = (TIME_ZERO[i]) + (char)(upTime_cpy % ((int)(moduli[i])));
				upTime_cpy /= (int)(moduli[i]);
			}
		}

		circ_write(term_out, time_str);

		circ_write(term_out, "\033[K\0338");
	}

	prev_val = new_val;
}
