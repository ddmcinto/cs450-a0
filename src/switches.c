#include "switches.h"
#include "circ_buf.h"

void switches_init(CircBuf *term_out, CircBuf *train_out) {
	circ_write(term_out, "\033[3g\0337\033[10;60HSW \033H\0338");

	/*
	//set the regular switches to curved
	char i;
	for (i = 2; i <= 18; ++i) {
		switches_set(train_out, i, 'c');
	}

	switches_set(train_out, 10, 's');
	switches_set(train_out, 17, 's');
	switches_set(train_out, 153, 's');
	switches_set(train_out, 154, 'c');
	switches_set(train_out, 155, 'c');
	switches_set(train_out, 156, 's');
	//*/
}

void switches_print(CircBuf *term_out) {
	int stati = switches_status(23, 0);
	circ_write(term_out, "\0337\033[9;60H\033[K\nSW 1 2 3 4 5 6 7 8 9\r\n\t");
	
	int i;
	for (i = 0x1; i < (1<<9); i <<= 1) {
		circ_putc(term_out, (stati & i) ? 'S' : 'C');
		circ_putc(term_out, ' ');
	}

	circ_write(term_out, "\r\n\t\033[K\n1 1 1 1 1 1 1 1 1\r\n\t");
	circ_write(term_out, 			   "0 1 2 3 4 5 6 7 8\r\n\t");

	for (; i < (1<<18); i <<= 1) {
		circ_putc(term_out, (stati & i) ? 'S' : 'C');
		circ_putc(term_out, ' ');
	}

	i = 1 << (152 & 0x1F);
	circ_write(term_out, "\r\n\t\033[K\n153: ");
	circ_putc(term_out, (stati & i) ? 'S' : 'C');
	i <<= 1;
	circ_write(term_out, " 154: ");
	circ_putc(term_out, (stati & i) ? 'S' : 'C');
	i <<= 1;
	circ_write(term_out, "\r\n\t155: ");
	circ_putc(term_out, (stati & i) ? 'S' : 'C');
	i <<= 1;
	circ_write(term_out, " 156: ");
	circ_putc(term_out, (stati & i) ? 'S' : 'C');

	circ_write(term_out, "\0338");
}

int switches_status(char sw, char dir) {
	--sw;
	static int stati = 0;

	if (dir == 'c') {
		stati &= ~( 1 << (sw & 0x1F));
	}
	else
	{
		stati |= ( 1 << (sw & 0x1F));
	}
	return stati;
}

void switches_set(CircBuf *train_out, char sw, char dir) {
	switches_status(sw, dir);

	circ_putc(train_out, (dir == 's') ? 0x21 : 0x22);
	circ_putc(train_out, sw);
}
